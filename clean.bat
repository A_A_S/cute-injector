@echo off

if exist release (
	del /F /S /Q release\*
	rmdir /S /Q release
)

if exist debug (
	del /F /S /Q debug\*
	rmdir /S /Q debug
)