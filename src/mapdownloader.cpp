#include <QDir>
#include <QFileInfo>
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFile>
#include <QSettings>
#include <QCryptographicHash>

#include "mapdownloader.h"
#include "settingswindow.h"
#include "const.h"

MapDownloader::MapDownloader(const MapArchive &archive, QObject *parent) :
	QObject(parent),
	manager(new QNetworkAccessManager(this)),
	archive(archive)
{
}

MapDownloader::~MapDownloader()
{
#ifdef _DEBUG
	qDebug() << Q_FUNC_INFO;
#endif
}

void MapDownloader::download()
{
#ifdef _DEBUG
	qDebug() << Q_FUNC_INFO;
#endif

	connect(manager, SIGNAL(finished(QNetworkReply*)), SLOT(downloadFinished(QNetworkReply*)));

	QNetworkReply *reply = manager->get(QNetworkRequest(QUrl(QString("%1%2.zip").arg(CI_FILEBASE_URL, archive.name()))));

	connect(reply, &QNetworkReply::downloadProgress,
		[=] (qint64 bytesReceived, qint64 bytesTotal){
			emit downloadProgress(bytesReceived, bytesTotal);
		});
}

void MapDownloader::downloadFinished(QNetworkReply *reply)
{
	if(reply->error() != QNetworkReply::NoError)
	{
#ifdef _DEBUG
		qDebug() << reply->errorString();
#endif
		emit finished(false, reply->errorString());
		return;
	}

	QSettings s;
	QFileInfo downloadDir = QFileInfo(s.value(SETTINGS_PATH_DOWNLOADS).toString());

	if(!downloadDir.exists())
	{
		if(!QDir().mkpath(downloadDir.absoluteFilePath()))
		{
			emit finished(false, QString("Cannot create '%1' direcotry to store downloaded files").arg(downloadDir.absoluteFilePath()));
			return;
		}
	}

	QString filePath = QDir(downloadDir.absoluteFilePath()).absoluteFilePath(QString("%1.zip").arg(archive.name()));
	QFile file(filePath);

	if(!file.open(QIODevice::ReadWrite))
	{
		emit finished(false, QString("Could not open %1 for writing").arg(file.fileName()));
		return;
	}

	file.write(reply->readAll());

	if(!file.reset())
	{
		emit finished(false, "Cannot reset file");
		return;
	}

	QCryptographicHash hash(QCryptographicHash::Md5);
	hash.addData(&file);
	file.close();

	if(archive.md5().compare(hash.result().toHex(), Qt::CaseInsensitive) != 0)
	{
		file.remove();
		emit finished(false, "MD5 hashsum mismatch");
		return;
	}

	emit finished(true, "Downloaded");
}
