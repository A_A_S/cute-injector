#include <QFile>
#include <QTextStream>

#include "utils.h"

QString getFileContent(const QString &fileName, bool *ok)
{
	QString s;
	QFile f(fileName);

	if(!f.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		if(ok != 0)
			*ok = false;

		return s;
	}

	QTextStream ts(&f);
	ts.setCodec("UTF-8");
	s = ts.readAll();
	f.close();

	if(ok != 0)
		*ok = true;

	return s;
}
