#ifndef WEBPAGE_H
#define WEBPAGE_H

#include <QWebEnginePage>

class WebPage : public QWebEnginePage
{
	Q_OBJECT
public:
	WebPage(QObject *parent = Q_NULLPTR);

	bool acceptNavigationRequest(const QUrl &url, NavigationType type, bool isMainFrame) Q_DECL_OVERRIDE;

signals:
	void linkClicked(const QUrl &);
};

#endif // WEBPAGE_H
