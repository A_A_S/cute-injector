#ifndef CONST_H
#define CONST_H

const QString CI_XML_URL = "https://www.quaddicted.com/reviews/quaddicted_database.xml";
const QString CI_MAP_IMG_URL = "https://www.quaddicted.com/reviews/screenshots/%1_injector.jpg";
const QString CI_FILEBASE_URL = "https://www.quaddicted.com/filebase/";
const QString CI_REVIEWS_URL = "https://www.quaddicted.com/reviews/";
const QString CI_XML_CACHE_FILENAME = "quaddicted_cache.xml";

#define _APP_VERSION 1
const int APP_VERSION = _APP_VERSION;

#define _APP_COMPANY "Quaddicted"
const QString APP_COMPANY = _APP_COMPANY;

#define _APP_NAME "CuteInjector"
const QString APP_NAME = _APP_NAME;

#endif // CONST_H
