#include <QObject>
#include <QXmlStreamReader>

#include "mapsmodel.h"


MapsModel::MapsModel(QObject *parent)
	: QAbstractTableModel(parent)
{
}

int MapsModel::columnCount(const QModelIndex &parent) const
{
	return parent.isValid() ? 0 : 4;
}

int MapsModel::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent)
	return this->maps.size();
}

QVariant MapsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if(orientation == Qt::Horizontal)
	{
		if(role == Qt::DisplayRole)
		{
			switch(section)
			{
				case 0:
					return "Name";
				case 1:
					return "Author";
				case 2:
					return "Title";
				case 3:
					return "Date";
			}
		}
	}

	return QVariant();
}

QVariant MapsModel::data(const QModelIndex &index, int role) const
{
	if(!index.isValid())
		return QVariant();

	if(index.row() >= maps.size() || index.row() < 0)
		return QVariant();

	if(role == Qt::DisplayRole || role == Qt::EditRole)
	{
		switch (index.column())
		{
			case 0:
				return maps.at(index.row()).name();
			case 1:
				return maps.at(index.row()).author();
			case 2:
				return maps.at(index.row()).title();
			case 3:
				return maps.at(index.row()).date();
		}
	}

	return QVariant();
}

void MapsModel::addMap(const MapArchive &map)
{
	//beginInsertRows(QModelIndex(), maps.size(), maps.size());
	this->maps.append(map);
	//endInsertRows();
}

MapArchive MapsModel::item(int row) const
{
	return maps.at(row);
}

MapArchive MapsModel::item(const QString &name) const
{
	return item(maps.indexOf(name));
}

void MapsModel::fillModel(QIODevice *device)
{
	QXmlStreamReader reader(device);
	bool isTechInfo = false; // Since this XML does not have namespaces we use this hack
	MapArchive map;

	while(!reader.atEnd() && !reader.hasError())
	{
		QXmlStreamReader::TokenType token = reader.readNext();

		if(token == QXmlStreamReader::StartElement)
		{
			if(reader.name() == "file")
			{
				if(!isTechInfo)
				{
					map.setName(reader.attributes().value("id").toString());
					map.setType(reader.attributes().value("type").toString());
					map.setRating(reader.attributes().value("rating").toInt());
				}
				else
				{
					map.addRequirement(reader.attributes().value("id").toString());
				}
			}

			if(reader.name() == "author")
			{
				map.setAuthor(reader.readElementText());
			}

			if(reader.name() == "title")
			{
				map.setTitle(reader.readElementText());
			}

			if(reader.name() == "md5sum")
			{
				map.setMd5(reader.readElementText());
			}

			if(reader.name() == "date")
			{
				// Work around Y2K ... some people still never heard of ISO8601
				QRegExp rx("(\\d{2})\\.(\\d{2})\\.(\\d{2})");
				int pos = rx.indexIn(reader.readElementText());

				if(Q_LIKELY(pos != -1))
				{
					int year = rx.cap(3).toInt() + 1900;
					if(year < 1996)
						year += 100;
					int month = rx.cap(2).toInt();
					int day = rx.cap(1).toInt();

					map.setDate(QDate(year, month, day));
				}
				else
				{
					map.setDate(QDate());
				}
			}

			if(reader.name() == "description")
			{
				map.setDescription(reader.readElementText(QXmlStreamReader::IncludeChildElements));
			}

			if(reader.name() == "techinfo")
			{
				isTechInfo = true;
			}

			if(reader.name() == "zipbasedir")
			{
				QString s = reader.readElementText();

				// Fix leading slash
				while(s.indexOf('/') == 0)
					s = s.mid(1, s.length() - 1);

				map.setZipbasedir(s);
			}

			if(reader.name() == "commandline")
			{
				map.setCommandline(reader.readElementText());
			}

			if(reader.name() == "startmap")
			{
				map.addMap(reader.readElementText());
			}
		}

		if(token == QXmlStreamReader::EndElement)
		{
			if(reader.name() == "techinfo")
			{
				isTechInfo = false;
			}

			if(reader.name() == "file" && !isTechInfo)
			{
				if(!isTechInfo)
				{
					this->addMap(map);
					map = MapArchive();
				}
			}
		}
	}
}
