TEMPLATE = app
TARGET = cuteinjector
QT += core gui widgets network webenginewidgets
CONFIG += c++11

CONFIG(debug, debug|release) {
	DEFINES += _DEBUG
	TARGET = $$join(TARGET,,,_debug)
}

win32 {
	# on windows we need Qt installed with sources
	INCLUDEPATH += $$[QT_INSTALL_HEADERS]/QtZlib
}
unix {
	LIBS += -lz
}

DEFINES += QUAZIP_STATIC
include($$PWD/../libs/quazip/quazip.pri)

SOURCES += \
	main.cpp\
	mainwindow.cpp \
	settingswindow.cpp \
	mapdownloader.cpp \
	utils.cpp \
	mapsmodel.cpp \
	maparchive.cpp \
    webpage.cpp

HEADERS += \
	mainwindow.h \
	settingswindow.h \
	const.h \
	mapdownloader.h \
	utils.h \
	mapsmodel.h \
	maparchive.h \
    webpage.h

FORMS += \
	mainwindow.ui \
	settingswindow.ui

RESOURCES += \
    cuteinjector.qrc
