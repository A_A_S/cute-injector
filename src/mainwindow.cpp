#include <QDate>
#include <QDesktopServices>
#include <QDir>
#include <QFileInfo>
#include <QMessageBox>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QProcess>
#include <QUrl>
#include <QWebEngineView>
#include <QWebEngineSettings>

#ifdef _DEBUG
	#include <QDebug>
#endif

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "webpage.h"
#include "mapdownloader.h"
#include "settingswindow.h"
#include "const.h"
#include "utils.h"
#include "JlCompress.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	mapsModel(new MapsModel(this)),
	mapsProxy(new QSortFilterProxyModel(this)),
	webViewHtmlTemplate(getFileContent(":/resources/preview.html")),
	playMenu(new QMenu(this))
{
	ui->setupUi(this);

	this->restoreGeometry(s.value(SETTINGS_UI_GEOMETRY).toByteArray());
	this->restoreState(s.value(SETTINGS_UI_STATE).toByteArray());

	ui->aViewShowLog->setChecked(s.value(SETTINGS_UI_SHOW_LOG).toBool());
	ui->log->setVisible(s.value(SETTINGS_UI_SHOW_LOG).toBool());
	ui->leftSplitter->restoreState(s.value(SETTINGS_UI_LEFT_SPLITTER_STATE).toByteArray());

	if(s.value(SETTINGS_UI_LEFT_SPLITTER_STATE).isNull())
		ui->leftSplitter->setSizes({10000, 100});

	ui->centralSplitter->restoreState(s.value(SETTINGS_UI_CENTRAL_SPLITTER_STATE).toByteArray());
	ui->webView->setPage(new WebPage());
	ui->webView->settings()->setAttribute(QWebEngineSettings::FocusOnNavigationEnabled, false);
	mapsProxy->setFilterCaseSensitivity(Qt::CaseInsensitive);

	this->loadDatabase();

	connect(ui->searchLine, SIGNAL(textEdited(const QString &)), this, SLOT(filterByAllColumns(QString)));
	connect(this, SIGNAL(mapActionTriggered(const QString &)), this, SLOT(startQuakeEngine(const QString &)));
	connect(ui->webView->page(), SIGNAL(linkClicked(QUrl)), this, SLOT(on_webView_linkClicked(QUrl)));
}

MainWindow::~MainWindow()
{
	s.setValue(SETTINGS_UI_GEOMETRY, this->saveGeometry());
	s.setValue(SETTINGS_UI_STATE, this->saveState());
	s.setValue(SETTINGS_UI_SHOW_LOG, ui->aViewShowLog->isChecked());
	s.setValue(SETTINGS_UI_LEFT_SPLITTER_STATE, ui->leftSplitter->saveState());
	s.setValue(SETTINGS_UI_CENTRAL_SPLITTER_STATE, ui->centralSplitter->saveState());

	delete ui;
}

void MainWindow::addLogMessage(const QString &message)
{
	ui->statusBar->showMessage(message, 5000);
	ui->log->appendPlainText(message);
}

void MainWindow::loadDatabase(bool forceCreateCache)
{
	bool createCache = forceCreateCache;
	QFileInfo fi = QFileInfo(QDir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation)).absoluteFilePath(CI_XML_CACHE_FILENAME));
	QDir cacheDir = QDir(fi.absolutePath());

	if(!cacheDir.exists())
		cacheDir.mkpath(fi.absolutePath());

	if(createCache || !fi.exists() || fi.lastModified().date() != QDate::currentDate())
	{
		createCache = true;
	}
	else
	{
		QFile f(fi.absoluteFilePath());

		if(f.open(QIODevice::ReadOnly))
		{
			this->mapsModel->fillModel(&f);
			f.close();
			this->setupTableView();
			this->addLogMessage("Database loaded from cache");
		}
		else
		{
			this->addLogMessage("Can not open cache file");
		}

		return;
	}

	QNetworkAccessManager *manager = new QNetworkAccessManager(this);

	connect(manager, &QNetworkAccessManager::finished, this,
		[=] (QNetworkReply *reply) {
			QFile f(fi.absoluteFilePath());
#ifdef _DEBUG
			qDebug() << reply->error();
#endif
			if(QNetworkReply::NoError == reply->error())
			{
				if(f.open(createCache ? QIODevice::ReadWrite : QIODevice::ReadOnly))
				{
					if(createCache)
					{
						f.write(reply->readAll());
						f.reset();
					}

					this->mapsModel->fillModel(&f);
					f.close();
					this->setupTableView();
					this->addLogMessage("Database loaded");
				}
				else
				{
					this->addLogMessage("Can not open cache file");
				}
			}
			else if(fi.exists())
			{
				if(f.open(QIODevice::ReadOnly))
				{
					this->mapsModel->fillModel(&f);
					f.close();
					this->setupTableView();
					this->addLogMessage("Can not retrieve database from remote host, loaded from cache");
				}
				else
				{
					this->addLogMessage("Can not retrieve database from remote host and cannot open cache file");
				}
			}
			else
			{
				this->addLogMessage("Can not retrieve database from remote host and cache file is not found");
			}

			manager->deleteLater();
		}
	);

	manager->get(QNetworkRequest(QUrl(CI_XML_URL)));
}

void MainWindow::setupTableView()
{
	this->mapsProxy->setSourceModel(this->mapsModel);
	this->mapsProxy->setSortCaseSensitivity(Qt::CaseInsensitive);
	this->mapsProxy->setSortLocaleAware(true);

	ui->maps->setModel(this->mapsProxy);
	ui->maps->horizontalHeader()->setSortIndicator(0, Qt::AscendingOrder);
	ui->maps->horizontalHeader()->setResizeContentsPrecision(-1);
	ui->maps->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);
	ui->maps->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);

	ui->maps->horizontalHeader()->resizeSection(0, 120);
	ui->maps->horizontalHeader()->resizeSection(1, 140);
	ui->maps->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
	ui->maps->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Fixed);
	ui->maps->horizontalHeader()->resizeSection(3, 80);

	connect(ui->maps->selectionModel(), SIGNAL(currentRowChanged(const QModelIndex &, const QModelIndex &)),
		this, SLOT(mapsCurrentRowChanged(const QModelIndex &, const QModelIndex &)), Qt::UniqueConnection);

	ui->maps->selectRow(0);
}

void MainWindow::mapsCurrentRowChanged(const QModelIndex &current, const QModelIndex &previous)
{
	Q_UNUSED(previous);

	ui->playButton->setMenu(Q_NULLPTR);
	ui->playButton->disconnect();

	if(current.isValid())
	{
		this->currentMapsModelId = this->mapsProxy->mapToSource(current).row();
		MapArchive archive = mapsModel->item(this->currentMapsModelId);
		QString html = this->webViewHtmlTemplate;
		html.replace("#TITLE#", archive.title());
		html.replace("#IMG_SRC#", QString(CI_MAP_IMG_URL).arg(archive.name()));
		html.replace("#IMG_ALT#", archive.name());
		html.replace("#DESCRIPTION#", archive.description());

		QString rd("none");

		if(!archive.requirements().isEmpty())
		{
			rd = "block";
			QString requirements;
			for(auto i: archive.requirements())
				requirements.append(QString("<li><a href=\"%1%2.html\">%2</a></li>").arg(CI_REVIEWS_URL ,i));

			html.replace("#REQUIREMENTS#", requirements);
		}

		html.replace("#REQUIREMENTS_DISPLAY#", rd);
		ui->webView->setHtml(html, QUrl(CI_REVIEWS_URL));

		QStringList maps = archive.maps().isEmpty() ? QStringList{archive.name()} : archive.maps();
		this->playMenu->clear();

		if(maps.size() == 1)
		{
			connect(ui->playButton, &QPushButton::clicked, this, [=]{
				emit this->mapActionTriggered(QString("%1 +map %2").arg(archive.commandline(), maps.first()));
			});
		}
		else if(maps.size() > 1)
		{
			for(auto m: maps)
			{
				QAction *action = new QAction(m, playMenu);
				connect(action, &QAction::triggered, [=]{
					emit this->mapActionTriggered(QString("%1 +map %2").arg(archive.commandline(), m));
				});
				this->playMenu->addAction(action);
			}

			ui->playButton->setMenu(this->playMenu);
			connect(ui->playButton, SIGNAL(clicked()), ui->playButton, SLOT(showMenu()));
		}
	}
}

void MainWindow::filterByFileId(const QString &fileId)
{
	mapsProxy->setFilterKeyColumn(0);
	mapsProxy->setFilterRegExp(QString("^%1$").arg(fileId));
	ui->maps->selectRow(0);
}

void MainWindow::filterByAllColumns(const QString &text)
{
	mapsProxy->setFilterKeyColumn(-1);
	mapsProxy->setFilterFixedString(text);
	ui->maps->selectRow(0);
}

void MainWindow::extractArchive(const MapArchive &archive)
{
	QSettings s;
	QString archiveFilePath = QDir(s.value(SETTINGS_PATH_DOWNLOADS).toString()).absoluteFilePath(QString("%1.zip").arg(archive.name()));
	QString destPath = QFileInfo(s.value(SETTINGS_PATH_ENGINE).toString()).absoluteDir().absoluteFilePath(archive.zipbasedir());
	QStringList filesCompressed = JlCompress::getFileList(archiveFilePath);
	QStringList filesExtracted = JlCompress::extractDir(archiveFilePath, destPath);

	if(filesCompressed.size() != filesExtracted.size())
	{
		this->addLogMessage("Extracted less files than compressed");
	}
	else
	{
		this->addLogMessage(QString("Archive %1.zip is successfully extracted to %2").arg(archive.name(), destPath));
	}
}

void MainWindow::startQuakeEngine(const QString & args)
{
	QProcess::startDetached(
		QFileInfo(s.value(SETTINGS_PATH_ENGINE).toString()).canonicalFilePath(),
		QString("%1 %2").arg(args, s.value(SETTINGS_MISC_ARGS).toString()).split(" ", QString::SkipEmptyParts, Qt::CaseInsensitive),
		QFileInfo(s.value(SETTINGS_PATH_ENGINE).toString()).absolutePath()
	);
}

void MainWindow::on_webView_linkClicked(const QUrl &url)
{
	// links are pretty inconsistent on quaddcited, so we try to work it around
	QUrl rurl = QUrl(CI_REVIEWS_URL);

	if(rurl.isParentOf(url))
	{
		QRegularExpression re("^/reviews/(?<mapname>.+)\\.html$");
		QString fileId = re.match(url.path()).captured("mapname");
		ui->searchLine->setText(fileId);
		this->filterByFileId(fileId);

		return;
	}

	QDesktopServices::openUrl(url);
}

void MainWindow::on_installButton_clicked()
{
	MapArchive archive = mapsModel->item(this->currentMapsModelId);
	QString archiveFilePath = QDir(s.value(SETTINGS_PATH_DOWNLOADS).toString()).absoluteFilePath(QString("%1.zip").arg(archive.name()));
	QFileInfo fi(archiveFilePath);

	if(fi.exists() && fi.isReadable())
	{
		QFile file(archiveFilePath);

		if(!file.open(QIODevice::ReadOnly))
		{
			this->addLogMessage(QString("Could not open %1 for reading").arg(file.fileName()));
			return;
		}

		QCryptographicHash hash(QCryptographicHash::Md5);
		hash.addData(&file);
		file.close();

		if(archive.md5().compare(hash.result().toHex(), Qt::CaseInsensitive) == 0)
		{
			this->addLogMessage("Map is already downloaded");
			this->extractArchive(archive);
			return;
		}
	}

	MapDownloader *md = new MapDownloader(archive, this);

	connect(md, &MapDownloader::downloadProgress, this,
		[=] (qint64 bytesReceived, qint64 bytesTotal) {
			ui->progressBar->setValue(int(bytesReceived * 100 / bytesTotal));
		});
	connect(md, &MapDownloader::finished, this,
		[=] (bool success, QString error) {
			md->deleteLater();
			ui->progressBar->reset();
			this->addLogMessage(error);

			if(success)
			{
				this->extractArchive(archive);
			}
		});

	md->download();
}

void MainWindow::on_aFileSettings_triggered()
{
	QSharedPointer<SettingsWindow> w(new SettingsWindow(this));
	w->exec();
}

void MainWindow::on_aFileReloadDatabase_triggered()
{
	this->loadDatabase(true);
}

void MainWindow::on_aHelpAbout_triggered()
{
	QString text =
		"CuteInjector is a cute alternative to QuakeInjector\n\n"
		"Version: %1\n";

	QMessageBox::about(this, "About CuteInjector", text.arg(QString::number(APP_VERSION)));
}
