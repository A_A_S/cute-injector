#include "maparchive.h"

MapArchive::MapArchive():
	_maps(QStringList()),
	_requirements(QStringList())
{

}

MapArchive::MapArchive(const QString &name):
	_maps(QStringList()),
	_requirements(QStringList())
{
	_name = name;
}

QString MapArchive::name() const
{
	return _name;
}

void MapArchive::setName(const QString &name)
{
	_name = name;
}

QString MapArchive::type() const
{
	return _type;
}

void MapArchive::setType(const QString &type)
{
	_type = type;
}

int MapArchive::rating() const
{
	return _rating;
}

void MapArchive::setRating(int rating)
{
	_rating = rating;
}

QString MapArchive::author() const
{
	return _author;
}

void MapArchive::setAuthor(const QString &author)
{
	_author = author;
}

QString MapArchive::title() const
{
	return _title;
}

void MapArchive::setTitle(const QString &title)
{
	_title = title;
}

QString MapArchive::md5() const
{
	return _md5;
}

void MapArchive::setMd5(const QString &md5)
{
	_md5 = md5;
}

quint32 MapArchive::size() const
{
	return _size;
}

void MapArchive::setSize(const quint32 &size)
{
	_size = size;
}

QDate MapArchive::date() const
{
	return _date;
}

void MapArchive::setDate(const QDate &date)
{
	_date = date;
}

QString MapArchive::description() const
{
	return _description;
}

void MapArchive::setDescription(const QString &description)
{
	_description = description;
}

QString MapArchive::zipbasedir() const
{
	return _zipbasedir;
}

void MapArchive::setZipbasedir(const QString &zipbasedir)
{
	_zipbasedir = zipbasedir;
}

QString MapArchive::commandline() const
{
	return _commandline;
}

void MapArchive::setCommandline(const QString &commandline)
{
	_commandline = commandline;
}

QStringList MapArchive::maps() const
{
	return _maps;
}

void MapArchive::setMaps(const QStringList &list)
{
	_maps = list;
}

void MapArchive::addMap(const QString &name)
{
	_maps.append(name);
}

QStringList MapArchive::requirements() const
{
	return _requirements;
}

void MapArchive::setRequirements(const QStringList &requirements)
{
	_requirements = requirements;
}

void MapArchive::addRequirement(const QString &requirement)
{
	_requirements.append(requirement);
}
