#include "webpage.h"

#ifdef _DEBUG
	#include <QDebug>
#endif

WebPage::WebPage(QObject *parent) :
	QWebEnginePage(parent)
{

}

bool WebPage::acceptNavigationRequest(const QUrl &url, QWebEnginePage::NavigationType type, bool isMainFrame)
{
	Q_UNUSED(type);
	Q_UNUSED(isMainFrame);

	emit linkClicked(url);

	return false;
}
