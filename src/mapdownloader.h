#ifndef MAPDOWNLOADER_H
#define MAPDOWNLOADER_H

#include <QNetworkAccessManager>
#include <QObject>
#include <QProgressBar>

#include <maparchive.h>

class MapDownloader : public QObject
{
	Q_OBJECT
public:
	explicit MapDownloader(const MapArchive &archive, QObject *parent = 0);
	~MapDownloader();
	void download();

private:
	QNetworkAccessManager *manager;
	MapArchive archive;

signals:
	void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);
	void finished(bool success, QString error);

private slots:
	void downloadFinished(QNetworkReply *reply);

};

#endif // MAPDOWNLOADER_H
