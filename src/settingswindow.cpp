#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <QStandardPaths>

#ifdef _DEBUG
	#include <QDebug>
#endif

#include "settingswindow.h"
#include "ui_settingswindow.h"

#include "const.h"

SettingsWindow::SettingsWindow(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::SettingsWindow)
{
	ui->setupUi(this);
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
	this->setWidgetValuesFromSettings();
}

SettingsWindow::~SettingsWindow()
{
	delete ui;
}

bool SettingsWindow::checkSettings()
{
	if(ui->downloadsDir->text().isEmpty())
	{
		QMessageBox::warning(this, "Warning", "Please specify correct maps download path");
		return false;
	}

	if(ui->enginePath->text().isEmpty())
	{
		QMessageBox::warning(this, "Warning", "Please specify engine executable");
		return false;
	}

	return true;
}

void SettingsWindow::saveSettings()
{
	s.setValue(SETTINGS_PATH_DOWNLOADS, ui->downloadsDir->text());
	s.setValue(SETTINGS_PATH_ENGINE, ui->enginePath->text());
	s.setValue(SETTINGS_MISC_ARGS, ui->engineArgs->text());
}

void SettingsWindow::setWidgetValuesFromSettings()
{
	ui->downloadsDir->setText(s.value(SETTINGS_PATH_DOWNLOADS).toString());
	ui->enginePath->setText(s.value(SETTINGS_PATH_ENGINE).toString());
	ui->engineArgs->setText(s.value(SETTINGS_MISC_ARGS).toString());
}

void SettingsWindow::on_downloadsButton_clicked()
{
	QString path = QFileDialog::getExistingDirectory(this, "Choose maps download directory");

	if(!path.isEmpty())
	{
		s.setValue(SETTINGS_PATH_DOWNLOADS, path);
		ui->downloadsDir->setText(s.value(SETTINGS_PATH_DOWNLOADS).toString());
	}
}

void SettingsWindow::on_quakeExePathButton_clicked()
{
	QString dir = s.value(SETTINGS_PATH_ENGINE, QDir(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)).absoluteFilePath(APP_NAME)).toString();

#ifdef Q_OS_WIN
	QString filter = "Executable files (*.exe)";
#else
	QString filter;
#endif
	QString path = QFileDialog::getOpenFileName(this, "Choose quake executable", dir, filter);

	if(!path.isEmpty())
	{
		s.setValue(SETTINGS_PATH_ENGINE, path);
		ui->enginePath->setText(s.value(SETTINGS_PATH_ENGINE).toString());
	}
}

void SettingsWindow::on_resetButton_clicked()
{
	ui->downloadsDir->setText(QDir(QStandardPaths::writableLocation(QStandardPaths::DownloadLocation)).absoluteFilePath(APP_NAME));
	ui->enginePath->clear();
	ui->engineArgs->clear();
}

void SettingsWindow::on_saveButton_clicked()
{
	if(checkSettings())
	{
		this->saveSettings();
		this->accept();
	}
}

void SettingsWindow::on_cancelButton_clicked()
{
	this->reject();
}
