#ifndef UTILS_H
#define UTILS_H

#include <QString>

QString getFileContent(const QString &fileName, bool *ok = 0);

#endif // UTILS_H
