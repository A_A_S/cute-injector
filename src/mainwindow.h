#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkReply>
#include <QSettings>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>

#include <mapsmodel.h>

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = Q_NULLPTR);
	~MainWindow();

private:
	Ui::MainWindow *ui;
	MapsModel *mapsModel;
	QSortFilterProxyModel *mapsProxy;
	const QString webViewHtmlTemplate;
	QMenu *playMenu;
	int currentMapsModelId = -1;
	QSettings s;

signals:
	void mapActionTriggered(const QString &args);

public slots:
	void addLogMessage(const QString &message);

private slots:
	void loadDatabase(bool forceCreateCache = false);
	void setupTableView();
	void mapsCurrentRowChanged(const QModelIndex & current, const QModelIndex & previous);
	void filterByFileId(const QString &fileId);
	void filterByAllColumns(const QString &text);
	void extractArchive(const MapArchive &archive);
	void startQuakeEngine(const QString &args);

	// Widgets
	void on_webView_linkClicked(const QUrl &url);

	// Buttons
	void on_installButton_clicked();

	// Menu
	void on_aFileSettings_triggered();
	void on_aFileReloadDatabase_triggered();

	void on_aHelpAbout_triggered();
};

#endif // MAINWINDOW_H
