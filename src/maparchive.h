#ifndef MAP_H
#define MAP_H

#include <QDate>
#include <QString>

class MapArchive
{
public:
	MapArchive();
	MapArchive(const QString &name);

	inline bool operator==(const MapArchive &other) const {return name() == other.name();}
	inline bool operator!=(const MapArchive &other) const {return !(*this == other);}
	inline bool operator==(const QString &other) const {return name() == other;}
	inline bool operator!=(const QString &other) const {return !(*this == other);}

	QString name() const;
	void setName(const QString &name);
	QString type() const;
	void setType(const QString &type);
	int rating() const;
	void setRating(int rating);
	QString author() const;
	void setAuthor(const QString &author);
	QString title() const;
	void setTitle(const QString &title);
	QString md5() const;
	void setMd5(const QString &md5);
	quint32 size() const;
	void setSize(const quint32 &size);
	QDate date() const;
	void setDate(const QDate &date);
	QString description() const;
	void setDescription(const QString &description);
	QString zipbasedir() const;
	void setZipbasedir(const QString &zipbasedir);
	QString commandline() const;
	void setCommandline(const QString &commandline);
	QStringList maps() const;
	void setMaps(const QStringList &list);
	void addMap(const QString &name);
	QStringList requirements() const;
	void setRequirements(const QStringList &requirements);
	void addRequirement(const QString &requirement);

protected:
	QString _name;
	QString _type;
	int _rating;
	QString _author;
	QString _title;
	QString _md5;
	quint32 _size;
	QDate _date;
	QString _description;
	QString _zipbasedir;
	QString _commandline;
	QStringList _maps;
	QStringList _requirements;
};

#endif // MAP_H
