#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <QDialog>
#include <QSettings>

const QString SETTINGS_PATH_DOWNLOADS = "/Paths/downloads";
const QString SETTINGS_PATH_ENGINE = "/Paths/engine";
const QString SETTINGS_MISC_ARGS = "/Misc/Args";
const QByteArray SETTINGS_UI_GEOMETRY = "/UI/Geometry";
const QByteArray SETTINGS_UI_STATE = "/UI/State";
const QByteArray SETTINGS_UI_SHOW_LOG = "/UI/ShowLog";
const QByteArray SETTINGS_UI_LEFT_SPLITTER_STATE = "/UI/LeftSplitterState";
const QByteArray SETTINGS_UI_CENTRAL_SPLITTER_STATE = "/UI/CentralSplitterState";

namespace Ui
{
	class SettingsWindow;
}

class SettingsWindow : public QDialog
{
	Q_OBJECT
public:
	explicit SettingsWindow(QWidget *parent = 0);
	~SettingsWindow();

private:
	Ui::SettingsWindow *ui;
	QSettings s;

	bool checkSettings();
	void saveSettings();
	void setWidgetValuesFromSettings();

private slots:
	void on_downloadsButton_clicked();
	void on_quakeExePathButton_clicked();

	void on_resetButton_clicked();
	void on_saveButton_clicked();
	void on_cancelButton_clicked();
};

#endif // SETTINGSWINDOW_H
