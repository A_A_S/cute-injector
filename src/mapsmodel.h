#ifndef MAPSMODEL_H
#define MAPSMODEL_H

#include <QAbstractTableModel>
#include <QIODevice>

#include "maparchive.h"

class MapsModel: public QAbstractTableModel
{
	Q_OBJECT

public:
	MapsModel(QObject *parent = 0);

	int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
	int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	void addMap(const MapArchive &map);
	MapArchive item(int row) const;
	MapArchive item(const QString &name) const;
	void fillModel(QIODevice *device);

private:
	QList<MapArchive> maps;
};

#endif // MAPSMODEL_H
