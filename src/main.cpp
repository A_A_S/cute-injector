#include <QApplication>

#include "mainwindow.h"
#include "const.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	QCoreApplication::setOrganizationName(APP_COMPANY);
	QCoreApplication::setApplicationName(APP_NAME);

	MainWindow w;
	w.show();

	return a.exec();
}
